﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player")) // checks for player
        {
            Destroy(other.gameObject); // destroys object
            GameManager.instance.alive = false; // makes then dead
            GameManager.instance.Lives--; // takes away a life
        }
    }
}
