﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour
{
    [SerializeField] private GameObject target;

    public float smoothSpeed;
    public Vector3 offset;

    void Start()
    {
    }

    void Update()
    {
        target = GameObject.FindWithTag("Player"); // looks for player tagged item
    }
    void LateUpdate()
    {
        Vector3 desiredPosition = target.transform.position + offset; // takes the position with the offset
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed); // sets the position by the position you want and does that by a set time for smoothness
        transform.position = smoothedPosition; // sets it to the position
    }
}
