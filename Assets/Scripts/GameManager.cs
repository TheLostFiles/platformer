﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Transform PTransform;
    public Transform spawnPoint;
    public GameObject Player;

    private GameObject playerInstance;


    public bool alive;
    public bool sceneSpawn;

    public int Lives = 4;


    void Awake()
    {
        if (instance == null) // checks if the instance is null
        {
            instance = this; // Store THIS instance of the class (component) in the instance variable
            DontDestroyOnLoad(gameObject); // Don't delete this object if we load a new scene
        }
        else
        {
            Destroy(this.gameObject); // There can only be one - this new object must die
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }


        
    }

    void Start()
    {
        
        //Spawn();

    }

    void Update()
    {
        if (Lives < 0) // checks if Lives is less than 0
        {
            lose(); // runs lose
            Lives = 1; // sets lives to 1
        }
          
    }

    void LateUpdate()
    {
        int currentScene = SceneManager.GetActiveScene().buildIndex; // sets the current scene to an int

        if (sceneSpawn == false) 
        {
            spawnPoint = GameObject.FindWithTag("spawnPoint").transform; // sets the spawn point
            Spawn(); // runs spawn
            sceneSpawn = true; // sets scenespawn to true

        }
        if (alive == false) 
        {
            Spawn();
        }
    }

    void Spawn()
    {
        playerInstance = Instantiate(Player, spawnPoint.transform.position, Quaternion.identity); // sets where she is going to spawn
        playerInstance.transform.position = new Vector3(playerInstance.transform.position.x, playerInstance.transform.position.y, 0);// makes a variable for where she spawns so she can be the correct size
        PTransform = playerInstance.transform; // makes the Player transform to where this mew imstamce 
        alive = true;
        
    }

    public void OnEnable()
    {
        SceneManager.sceneLoaded += onlevelFinishedLoading; // sets up the screen load checking
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= onlevelFinishedLoading;
    }

    void onlevelFinishedLoading(Scene scene, LoadSceneMode mode) // checks when the level has finished loading
    {
        sceneSpawn = false;
    }

    void lose()
    {
        SceneManager.LoadScene(5); // loads the lose scene
    }
}
