﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other) // checks for trigger entering
    {
        if (other.CompareTag("Player")) // gets The tag of the player
        {
            GameManager.instance.spawnPoint = transform; // sets the spawn point to the checkpoint
        }
    }
}
