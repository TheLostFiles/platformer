﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundLoop : MonoBehaviour
{
    public GameObject[] levels;
    private Camera mainCamera;
    private Vector2 screenBounds;
    public float choke;

    private Vector3 lastScreenPosition;


    // Start is called before the first frame update
    void Start()
    {
        mainCamera = gameObject.GetComponent<Camera>();
        screenBounds = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width * 2, Screen.height, mainCamera.transform.position.z));
        foreach (GameObject obj in levels) // makes local reference that holds the value of the current row in the list
        {
            loadChildObjects(obj); // goes through the list of sprites and runs function for every one
        }

        lastScreenPosition = transform.position;
    }

    void loadChildObjects(GameObject obj)
    {
        float objectWidth = obj.GetComponent<SpriteRenderer>().bounds.size.x - choke; // gets horizontal boundary box of the sprite
        int childsNeeded = (int)Mathf.Ceil(screenBounds.x * 2 / objectWidth); // figures how many clones to make to fill screen
        GameObject clone = Instantiate(obj) as GameObject; // clones object
        for (int i = 0; i <= childsNeeded; i++) // creates child objects
        {
            GameObject c = Instantiate(clone) as GameObject; // makes clone
            c.transform.SetParent(obj.transform); // makes a child of parent
            c.transform.position = new Vector3(objectWidth * i, obj.transform.position.y - 15 , obj.transform.position.z); // spaces them out correctly 
            c.name = obj.name + i; // names names so things are clean
        }
        Destroy(clone); // destroys clone
        Destroy(obj.GetComponent<SpriteRenderer>()); // destroys the component
    }

    void repositionChildObjects(GameObject obj) 
    {
        Transform[] children = obj.GetComponentsInChildren<Transform>(); // Gets a transform for the child components
        if (children.Length > 1) // checks length
        {
            GameObject firstChild = children[1].gameObject; // makes the first child the one
            GameObject lastChild = children[children.Length - 1].gameObject; // sets the last child to the last
            float halfObjectWidth = lastChild.GetComponent<SpriteRenderer>().bounds.extents.x; // Grabs only half the width 

            if (transform.position.x + screenBounds.x > lastChild.transform.position.x + halfObjectWidth) // check if the camera is going far enough that you can see outside the background
            {
                firstChild.transform.SetAsLastSibling(); // switchs the first child to last child
                firstChild.transform.position = new Vector3(lastChild.transform.position.x + halfObjectWidth * 2, lastChild.transform.position.y, lastChild.transform.position.z); // swaps their position
                 
            }else if(transform.position.x - screenBounds.x < firstChild.transform.position.x - halfObjectWidth) // check if the camera is going far enough that you can see outside the background
            {
                lastChild.transform.SetAsFirstSibling();// switchs the first child to last child
                lastChild.transform.position = new Vector3(firstChild.transform.position.x - halfObjectWidth * 2, firstChild.transform.position.y, firstChild.transform.position.z); // swaps their position
            }
        }
    }

    void LateUpdate()
    {
        foreach (GameObject obj in levels) 
        {
            repositionChildObjects(obj); 
            float parallaxSpeed = Mathf.Clamp01(Mathf.Abs(transform.position.z / obj.transform.position.z)); // sets the parallax speed
            float difference = transform.position.x - lastScreenPosition.x; // sets the difference
            obj.transform.Translate(Vector3.right * difference * parallaxSpeed); // makes the obj move
        }
        lastScreenPosition = transform.position; // sets the last screen position to this one
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
