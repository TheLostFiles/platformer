﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private SpriteRenderer sr;
    private Transform tf;
    public Animator Animator;

    public float Speed;
    public float Jumpforce;

    private bool isGrounded;
    private bool facingRight;

    private int extraJumps;
    public int Jumps;

    private float Movement;
    private float Climb;


    void Start()
    {
        extraJumps = Jumps; // sets extrajumps to  jumps
        tf = GetComponent<Transform>();
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
    }


    void Update()
    {
        Movement = Input.GetAxis("Horizontal") * Speed; // movement 
        rb2d.velocity = new Vector2(Movement, rb2d.velocity.y); // movement still


        Animator.SetFloat("Speed", Mathf.Abs(Movement)); // sets float in the animator


        if (Input.GetButtonDown("Jump") && extraJumps > 0) // checks for pressing space and in you hva enough jumps
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, Jumpforce); // makes the player jump
            isGrounded = false; // isGrounded = false
            extraJumps--; // takes away a jump
        }

        if (isGrounded == false)
        {
            Animator.SetBool("isJumping", true); // sets jumping bool to true
        }
        else
        {
            Animator.SetBool("isJumping", false);// sets jumping bool to false
            extraJumps = Jumps; // makes jumps extra jumps
        }

        if (!facingRight && Movement < 0) // check for moving left
        {
            flip(); // flip
        }
        else if(facingRight && Movement > 0)// check for moving right
        {
            flip(); // flip
        }
        
    }

    void flip()
    {
        facingRight = !facingRight; // makes it not itself 
        Vector3 scaler = transform.localScale; // makes scaler
        scaler.x *= -1; // swaps which way she looks
        transform.localScale = scaler; // sets player scale to scaler
    }


    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.CompareTag("Flooring")) // checks floor
        {
            isGrounded = true; 
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Flooring"))  // checks floor
        {
            isGrounded = false;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Ladders")) // checks for ladders
        {
            if (Input.GetAxis("Vertical") >= 0.1f) // if you press up while in a ladder
            {
                
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // goes to the next scene
                GameManager.instance.OnEnable(); // runs on enable
            }
        }

        if (other.CompareTag("Win")) // checks for win
        {
            if (Input.GetAxis("Vertical") >= 0.1f) // checks if you are pressing up 
            {
                SceneManager.LoadScene(4); // loads win scene
                SceneManager.LoadScene(4); // loads win scene
            }
        }

    }
}
